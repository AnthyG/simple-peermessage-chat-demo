# Simple PeerMessage Chat Demo

Clone this repo into a new zite, edit the `ignore` property in it's `content.json` to look like follows:

`"ignore": "((js|css)/(?!all.(js|css))|.git)"`

And voilá :)