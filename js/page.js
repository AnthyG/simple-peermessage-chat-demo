class Page extends ZeroFrame {
    sendMessage(text) {
        this.cmd("peerBroadcast", {
            message: {
                text: text,
                timestamp: new Date().valueOf()
            },
            privatekey: false
        }); // send message "text" to everybody
        messageInput.value = "";
    }

    setSiteInfo(site_info) {
        let out = document.getElementById("out");
        out.innerHTML =
            "Page address: " + site_info.address +
            "<br>- Peers: " + site_info.peers +
            "<br>- Size: " + site_info.settings.size +
            "<br>- Modified: " + (new Date(site_info.content.modified * 1000));
    }

    onOpenWebsocket() {
        this.cmd("siteInfo", [], function(site_info) {
            page.setSiteInfo(site_info);
        });
    }

    onRequest(cmd, message) {
        if (cmd == "setSiteInfo") {
            this.setSiteInfo(message.params);
        } else if (cmd == "peerReceive") { // and it's from PeerMessage plugin
            let text = decodeURIComponent(message.params.message.text);
            let timestamp = message.params.message.timestamp;
            let identityAddress = message.params.signed_by;
            let userName = message.params.cert;

            console.log("onRequest::peerReceive", message);

            let el = document.createElement("li");
            el.appendChild(document.createTextNode(new Date(timestamp) + " >> " + userName + ": "));
            el.appendChild(document.createTextNode(text));
            messageHistory.insertBefore(el, messageHistory.firstChild);

            this.cmd("peerValid", [message.params.hash]); // This message is correct - broadcast to other peers
        } else {
            this.log("Unknown incoming message:", cmd);
        }
    }
}

page = new Page();

const messageSend = document.getElementById("messageSend")
const messageInput = document.getElementById("messageInput");
const messageHistory = document.getElementById("messageHistory");

messageSend.onclick = function() {
    page.sendMessage(encodeURIComponent(messageInput.value.trim()));
};

const changeCertBtn = document.getElementById("changeCertBtn");
changeCertBtn.onclick = function() {
    page.cmd("certSelect", [
        [
            "zeroid.bit",
            "kaffie.bit",
            "cryptoid.bit",
            "peak.id",
            "zeroverse.bit",
            "kxoid.bit"
        ]
    ]);
};